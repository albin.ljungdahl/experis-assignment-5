/**
 * Decodes a text encoded in the base64 format.
 */
const decode = (text) => {
  var txt = document.createElement("textarea");
  txt.innerHTML = text;
  return txt.value;
};

export default decode;