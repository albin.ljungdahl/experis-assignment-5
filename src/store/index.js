import Vue from "vue";
import Vuex from "vuex";

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    difficulty: 0,
    numberOfQuestions: 1,
    category: 0,
    score: 0,
    questions: [],
    questionsLoaded: false,
    questionIndex: 0,
    userAnswers: [],
  },
  mutations: {
    setDifficulty: (state, payload) => {
      state.difficulty = payload;
    },
    setNumberOfQuestions: (state, payload) => {
      state.numberOfQuestions = payload;
    },
    setCategory: (state, payload) => {
      state.category = payload;
    },
    setScore: (state, payload) => {
      state.score = payload;
    },
    addToScore: (state, payload) => {
      state.score += payload;
    },
    setQuestions: (state, payload) => {
      state.questions = payload;
    },
    setQuestionsLoaded: (state, payload) => {
      state.questionsLoaded = payload;
    },
    setQuestionIndex: (state, payload) => {
      state.questionIndex = payload;
    },
    incrementQuestionIndex: (state, payload) => {
      state.questionIndex += payload;
    },
    setUserAnswers: (state, payload) => {
      state.userAnswers = payload;
    },
    pushToUserAnswers: (state, payload) => {
      state.userAnswers.push(payload);
    }
  },
  actions: {
    /**
     * Fetches questions from the API based on the difficulty, category and number of questions the user chose.
     */
    async fetchQuestions({ state, commit }) {
      const questions = state.numberOfQuestions;
      const category = state.category !== 0 ? `&category=${state.category}` : "";
      const difficulty = state.difficulty !== 0 ? `&difficulty=${state.difficulty}` : "";

      const response = await fetch(`https://opentdb.com/api.php?amount=${questions}${category}${difficulty}`);
      const data = await response.json();

      commit("setQuestionsLoaded", true);

      if (data.response_code === 0) {
        commit("setQuestions", data.results);
      }

      else if (data.response_code === 1) {
        throw new Error("There are not enough questions with the chosen specifications...");
      }

      else if (data.response_code === 2) {
        throw new Error("Invalid parameter...");
      }

      else {
        throw new Error("Something went wrong when fetching questions...");
      }
    },

    /**
     * Check if the user's chosen answer is correct.
     */
    answer({ state, commit }, payload) {
      const correctAnswer = state.questions[state.questionIndex].correct_answer;
      if (payload === correctAnswer) {
        commit("addToScore", 10);
        alert(`Correct! Your score is ${state.score}`);
      }

      else {
        alert("Incorrect!");
      }

      commit("pushToUserAnswers", payload);
      commit("incrementQuestionIndex", 1);
    },

    /**
     * Reset all states to their original values.
     */
    reset({ commit }) {
      commit("setDifficulty", 0);
      commit("setNumberOfQuestions", 1);
      commit("setCategory", 0);
      commit("setScore", 0);
      commit("setQuestions", []);
      commit("setQuestionsLoaded", false);
      commit("setQuestionIndex", 0);
      commit("setUserAnswers", []);
    },

    /**
     * Reset all states to their original values except the chosen difficulty, number of questions and category.
     */
    playAgain({ commit }) {
      commit("setScore", 0);
      commit("setQuestions", []);
      commit("setQuestionsLoaded", false);
      commit("setQuestionIndex", 0);
      commit("setUserAnswers", []);
    }
  },
  getters: {
    /**
     * Returns all the alternatives for the current question.
     */
    alternatives: (state) => {
      return state.questions[state.questionIndex].incorrect_answers.concat(state.questions[state.questionIndex].correct_answer);
    },

    /**
     * Returns the current question.
     */
    currentQuestion: (state) => {
      return state.questions[state.questionIndex].question;
    }
  }
});
