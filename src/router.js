import VueRouter from "vue-router";
import StartPage from "./views/StartPage";
import QuestionPage from "./views/QuestionPage";
import ResultPage from "./views/ResultPage";

const routes = [
  {
    path: "/",
    name: "Start Page",
    component: StartPage
  },

  {
    path: "/questions",
    name: "Questions",
    component: QuestionPage
  },

  {
    path:"/result",
    name: "Result",
    component: ResultPage
  }
];

const router = new VueRouter({ routes });

export default router;