# Trivia Game
A simple Trivia Game using the Vue.js framework.

## Setup
Clone the project, run `npm install` to install the required dependencies, and `npm run serve` to run the application locally.

## Link
Link to the deployed app: https://stark-beyond-78715.herokuapp.com/

## Members
Albin Ljungdahl & Alexander Idemark
